from underthesea import sentiment
import sys

inp = sys.argv[1];

if sentiment(inp) == "negative":
    print(0)
else:
    print(1)