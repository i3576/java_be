from flask import Flask, jsonify, request
import pandas as pd
import pickle
import sys

df = pd.read_csv(r'D:\product.csv')
loaded_model = pickle.load(open(r'D:\model.pkl', 'rb'))

def recommend(index_of_product):

    similarity_score = list(enumerate(loaded_model[index_of_product]))

    sorted_similar_product = sorted(similarity_score, key=lambda x: x[1], reverse=True)

    list_recommendation = []

    for item in sorted_similar_product:
        if(float(item[1]) == 0):
            break 
        index = item[0]
        index_of_recommend = df[index == df.index]['_id'].values[0]

        list_recommendation.append(index_of_recommend)
    return list_recommendation


def recommendProduct(product_id):
    index_of_product = df[product_id == df._id]['index'].values[0]
    # res = jsonify(recommend(int(index_of_product)))
    return recommend(int(index_of_product))

inp = sys.argv[1];

lst = recommendProduct(inp)

for item in lst:
    print(item)