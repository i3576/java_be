package com.example.Java_BE.model;

import org.springframework.data.annotation.Id;

public class Category{

    public Category() {
    }

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Id
    private String _id;
    private String name;

}
