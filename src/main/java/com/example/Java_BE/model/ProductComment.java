package com.example.Java_BE.model;

import org.springframework.data.annotation.Id;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ProductComment {
    @Id
    private String _id;
    private String ownerId;
    private String content;
    private String productId;
    private Date createdAt;
    private Date updatedAt;
    private ReplyComment reply;
}
