package com.example.Java_BE.model;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Mess {
    private String senderName;
    private String receiverName;
    private String message;
    private String date;
    private String status;
}