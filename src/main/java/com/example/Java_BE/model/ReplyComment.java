package com.example.Java_BE.model;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReplyComment {
    private String _id;
    private String ownerId; //này phải là quản trị viên mới đc
    private String content;
    private String productCommentId;
    private Date createdAt;
    private Date updatedAt;
}
