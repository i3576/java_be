package com.example.Java_BE.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Rate {
    @Id
    private String _id;
    private String userId;

    private String productId;
    private float rateStar;
    private String content;
    private Date createAt;
    private Object sentimentRerult;

    public Rate() {
    }

    public Rate(String userId, String productId, float rateStar, String content, Date createAt, Object sentiment) {
        this.userId = userId;
        this.productId = productId;
        this.rateStar = rateStar;
        this.content = content;
        this.createAt = createAt;
        this.sentimentRerult = sentiment;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public float getRateStar() {
        return rateStar;
    }

    public void setRateStar(float rateStar) {
        this.rateStar = rateStar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Object getSentimentRerult() {
        return sentimentRerult;
    }

    public void setSentimentRerult(Object sentimentRerult) {
        this.sentimentRerult = sentimentRerult;
    }
}
