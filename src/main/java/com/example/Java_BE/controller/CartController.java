package com.example.Java_BE.controller;

import com.example.Java_BE.model.Cart;
import com.example.Java_BE.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class CartController {
    @Autowired
    private CartService cartService;

    @GetMapping("/v2/cart")
    public @ResponseBody
    Object getAllCart() {
        return cartService.getAllCart();
    }

    @GetMapping("/v2/cart/get-all-cart")
    public @ResponseBody
    Object getAllCartInfo() {
        return cartService.getAllCartInfo();
    }

    @PostMapping("/v2/cart/add-cart")
    public @ResponseBody
    Object addCart(@RequestBody Cart cart) {
        return cartService.addCart(cart);
    }

    @PutMapping("/v2/cart/update-cart")
    public @ResponseBody
    Object updateCart(@RequestBody Cart cart) {
        return cartService.updateCart(cart);
    }

    @GetMapping("/v2/cart/{id}")
    public @ResponseBody
    Object getCart(@RequestBody Cart cart) {
        return null;
    }

    @DeleteMapping("/v2/cart/{id}")
    public @ResponseBody
    Object deleteCart(@PathVariable String id) {
        return cartService.deleteCart(id);
    }
}
