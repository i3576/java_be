package com.example.Java_BE.controller;

import com.example.Java_BE.payload.request.ChangePasswordReq;
import com.example.Java_BE.payload.request.ResetPasswordReq;
import com.example.Java_BE.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.MessagingException;

@Controller
public class AuthController {
    @Autowired
    private AuthService authService;

    @PutMapping("/api/v2/update-password")
    public @ResponseBody
    Object updatePassword(@RequestBody ChangePasswordReq data) {
        return authService.updatePassword(data);
    }

    @PostMapping("/reset-password")
    public @ResponseBody
    Object resetPassword(@RequestBody ResetPasswordReq data) throws MessagingException {
        return authService.resetPassword(data);
    }
}
