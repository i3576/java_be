package com.example.Java_BE.controller;

import com.example.Java_BE.payload.request.MomoPaymentReq;
import com.example.Java_BE.payload.request.MomoPaymentResultReq;
import com.example.Java_BE.service.MomoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class MomoController {
    @Autowired
    private MomoService momoService;

    @RequestMapping(value = "/v1/create-payment", method = RequestMethod.POST)
    @ResponseBody
    Object paymentResponse(@RequestBody MomoPaymentReq req) {
        return momoService.createPayment(req);
    }

    @RequestMapping(value = "/v1/payment-result", method = RequestMethod.POST)
    @ResponseBody
    Object paymentResultRespone(MomoPaymentResultReq req) { return momoService.paymentResult(req);}
}
