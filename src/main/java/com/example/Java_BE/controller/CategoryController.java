package com.example.Java_BE.controller;

import com.example.Java_BE.model.Category;
import com.example.Java_BE.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/v1/category")
    public @ResponseBody
    Object getExistCategories() {
        return categoryService.getExistCategories();
    }

    @PostMapping("/v4/category/add-category")
    public @ResponseBody
    Object addCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }


    @PostMapping("/v1/category/add-category")
    public @ResponseBody
    Object addCategoryTest(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }
}
