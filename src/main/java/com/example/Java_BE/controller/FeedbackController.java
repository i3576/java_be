package com.example.Java_BE.controller;

import com.example.Java_BE.model.Order;
import com.example.Java_BE.payload.request.AddFeedBackReq;
import com.example.Java_BE.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
public class FeedbackController {
    @Autowired
    FeedbackService feedbackService;

    @PostMapping("/v2/feedback/add-feedback/{id}")
    public @ResponseBody
    Object addFeedback(@RequestBody List<AddFeedBackReq> req, @PathVariable String id) {
        return feedbackService.addFeedback(req,id);
    }
    @GetMapping("/v1/feedback/get-all-feedback-of-product/{id}")
    public @ResponseBody
    Object getAllFeedbackOfProduce(@PathVariable String id) {
        return feedbackService.getAllFeedbackOfProduct(id);
    }
    @GetMapping("/v4/feedback/get-all-feedback-for-admin")
    public  @ResponseBody
    Object getAllFeedbackForAdmin() {
        return feedbackService.getAllFeedbackForAdmin();
    }
}
