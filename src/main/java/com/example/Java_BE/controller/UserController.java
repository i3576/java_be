package com.example.Java_BE.controller;

import com.example.Java_BE.model.CustomUser;
import com.example.Java_BE.service.UserServiceImplements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserServiceImplements userService;

    @GetMapping("/v2/user")
    public @ResponseBody
    Object getInfo() {
        return userService.getInfo();
    }

    @PutMapping("/v2/user/update")
    public @ResponseBody
    Object updateUserInfo(@RequestBody CustomUser user) {
        return userService.updateUserInfo(user);
    }

    @GetMapping("/v3/user/get-all-staff")
    public @ResponseBody
    Object getAllStaff()
    {
        return userService.getAllStaff();
    }
}
