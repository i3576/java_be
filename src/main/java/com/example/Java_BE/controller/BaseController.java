package com.example.Java_BE.controller;

import com.example.Java_BE.config.SecurityConfig;
import com.example.Java_BE.model.*;
import com.example.Java_BE.payload.request.JwtUserInfo;
import com.example.Java_BE.payload.request.LoginReq;
import com.example.Java_BE.payload.request.RegisterReq;
import com.example.Java_BE.payload.response.JwtResponse;
import com.example.Java_BE.service.EmailSenderService;
import com.example.Java_BE.service.UserServiceImplements;
import com.example.Java_BE.utility.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

@Controller
public class BaseController {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserServiceImplements userService;

    @Autowired
    private EmailSenderService senderService;

    @Autowired
    SecurityConfig securityConfig;

    @Autowired
    PasswordEncoder encoder;

    @PostMapping("/register")
    @ResponseBody
    public Object register(@RequestBody RegisterReq user) throws Exception {

        if (userService.isExistUserName(user.getUsername())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Tên tài khoản đã tồn tại");
            return map;
        }
        if (userService.isExistEmail(user.getEmail())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Email đã tồn tại");
            return map;
        }
        String ePassword = encoder.encode(user.getPassword());
        mongoTemplate.save(new CustomUser(user.getEmail(), user.getUsername(), ePassword, "ROLE_USER", user.getPhone(), user.getAddress(), user.getFullName()));

        final JwtUserInfo userDetails
                = userService.getUserByUsername(user.getUsername());

        final String token =
                jwtUtility.generateToken(userDetails);

        return new JwtResponse(token);
    }

    @PostMapping("/login")
    @ResponseBody
    public Object login(@RequestBody LoginReq user) throws Exception {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final JwtUserInfo userDetails
                = userService.getUserByUsername(user.getUsername());

        final String token =
                jwtUtility.generateToken(userDetails);

        return new JwtResponse(token);
    }

    @GetMapping("/api/v1/product/getAll")
    public @ResponseBody
    Object check1() {
        return mongoTemplate.findAll(Product.class);
    }


    @PostMapping("/api/v1/feedback")
    public @ResponseBody
    Object sentimentAnalysis(@RequestBody String rating) {

        return analiseSentiment(rating);
    }

    @GetMapping("/api/v1/product/{id}/recommend")
    public @ResponseBody
    Object recommendByProductId(@PathVariable String id) {
        return recommend(id);
    }

    public Object analiseSentiment(String message) {
        try{
            // path to sentimentAnalysis python file
            String pythonPath = "F:\\sentimentAnalysis.py";

            // path to python.exe
            String pythonExe = "C:\\Users\\Admin\\AppData\\Local\\Programs\\Python\\Python310\\python.exe";

            // run python file and pass feedback message
            ProcessBuilder pb = new ProcessBuilder(pythonExe,
                    "-u",
                    pythonPath,
                    message);
            Process p = pb.start();

            // read value returned from python file
            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            line = bfr.readLine();

            return Collections.singletonMap("res",line.equals("0")?"Đánh giá tiêu cực":"Đánh giá tích cực" ) ;
        }catch(Exception e){System.out.println(e);}

        return -1;
    }

    public Object recommend(String id) {
        try{
            // path to recommend python file
            String pythonPath = "F:\\recommend.py";

            // path to python.exe
            String pythonExe = "C:\\Users\\Admin\\AppData\\Local\\Programs\\Python\\Python310\\python.exe";

            // run python file and pass feedback message
            ProcessBuilder pb = new ProcessBuilder(pythonExe,
                    "-u",
                    pythonPath,
                    id);
            Process p = pb.start();

            // read value returned from python file
            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            line = bfr.readLine();

            List<String> lst = new ArrayList<>();
            List<Object> result = new ArrayList<>();

            while (bfr.ready()) {
                lst.add(bfr.readLine());
            }

            for(String item : lst)
            {
                result.add(mongoTemplate.findById(item, Product.class));
            }
            return result;
        }catch(Exception e){System.out.println(e);}

        return -1;
    }
}
