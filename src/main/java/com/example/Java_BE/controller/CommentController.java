package com.example.Java_BE.controller;

import com.example.Java_BE.model.Mess;
import com.example.Java_BE.model.ProductComment;
import com.example.Java_BE.model.ReplyComment;
import com.example.Java_BE.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;

@Controller
public class CommentController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private CommentService commentService;

    @MessageMapping("/message")
    @SendTo("/chatroom/public")
    public Mess receiveMessage(@Payload Mess message){
        return message;
    }

    @MessageMapping("/product/{productId}/**")
    public ProductComment sendProductComment(@DestinationVariable String productId, @Payload ProductComment cmt){
        commentService.createComment(cmt);
        simpMessagingTemplate.convertAndSend("/product/" + productId + "/**", cmt);
        simpMessagingTemplate.convertAndSend("/product", cmt);

        return cmt;
    }

    @MessageMapping("/delete")
    public ProductComment deleteProductComment(@Payload ProductComment cmt){
        commentService.deleteComment(cmt);
        simpMessagingTemplate.convertAndSend("/product/" + cmt.getProductId() + "/**", cmt);
        simpMessagingTemplate.convertAndSend("/product", cmt);
        return cmt;
    }

    @GetMapping("/api/v1/get-all-comment")
    @ResponseBody
    public Object getAllComment(@RequestParam String productId) {
        return commentService.getAllComment(productId);
    }

    @GetMapping("/api/v3/get-all-comment-staff")
    @ResponseBody
    public Object getAllCommentStaff() {
        return commentService.getAllCommentStaff();
    }

    @PostMapping("/api/v3/reply")
    @ResponseBody
    public Object createCommentReply(@RequestBody ReplyComment cmt) {
        commentService.createCommentReply(cmt);
        return Collections.singletonMap("statusCode", 201);
    }
}
