package com.example.Java_BE.controller;

import com.example.Java_BE.model.Specification;
import com.example.Java_BE.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class SpecificationController {
    @Autowired
    SpecificationService specificationService;

    @GetMapping("/v1/specification/info/{id}")
    public @ResponseBody
    Object getSpecificationById(@PathVariable String id) {
        return specificationService.getSpecificationById(id);
    }

    @PostMapping("/v4/specification/add-specification")
    public @ResponseBody
    Object addSpecification(@RequestBody Specification specification) {
        return specificationService.addSpecification(specification);
    }

    @PutMapping("/v4/specification/update-specification")
    public @ResponseBody
    Object updateSpecification(@RequestBody Specification specification) {
        return specificationService.updateSpecification(specification);
    }

    @DeleteMapping("/v4/specification/{id}")
    public @ResponseBody
    Object deleteSpecification(@PathVariable String id) {
        return specificationService.deleteSpecification(id);
    }

    @GetMapping("/v4/specification/detail")
    public @ResponseBody
    Object getAllSpecification() {
        return specificationService.getAllSpecification();
    }
}
