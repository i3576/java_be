package com.example.Java_BE.controller;

import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;
import com.example.Java_BE.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductService productService;

    //test
    @PostMapping("/v1/product/add")
    public @ResponseBody
    Object addProductTest(@RequestBody Product product) {
        return productService.addProduct(product, product.getCategoryId());
    }

    @PostMapping("/v1/specification/add")
    public @ResponseBody
    Object addSpecificationTest(@RequestBody Specification specification) {
        return productService.addSpecification(specification, specification.getProductId());
    }

    @GetMapping("/v1/product/get-outstanding")
    public @ResponseBody
    Object getTop8Sell() {
        return productService.getTop8Sell();
    }

    @GetMapping("/v1/product")
    public @ResponseBody
    Object getAllProducts() {
        return productService.getAllProduct();
    }

    @GetMapping("/v1/product/info/{id}")
    public @ResponseBody
    Object getInfoByProductID(@PathVariable String id) {
        return productService.getInfoByProductID(id);
    }

    @GetMapping("/v1/product/{id}")
    public @ResponseBody
    Object getSpecificationForProductID(@PathVariable String id) {
        return productService.getSpecificationForProductID(id);
    }

    @GetMapping("/v1/specification/{id}")
    public @ResponseBody
    Object getProductBySpecificationID(@PathVariable String id) {
        return productService.getProductBySpecificationId(id);
    }

    @GetMapping("/v1/product/filter")
    @ResponseBody
    public Object getProductByParams(@RequestParam(required = false) String category,
                                           @RequestParam(required = false) Integer minPrice,
                                           @RequestParam(required = false) Integer maxPrice,
                                           @RequestParam(required = false) Float minRating,
                                           @RequestParam(required = false) Float maxRating,
                                           @RequestParam(required = false) Integer minBattery,
                                           @RequestParam(required = false) Integer maxBattery) {
        return productService.getProductByParams(category,minPrice,maxPrice,minRating,maxRating,minBattery,maxBattery);
    }

    //user

    //admin
    @DeleteMapping("/v3/product/{id}")
    public @ResponseBody
    Object deleteProduct(@PathVariable String id){
        return productService.deleteProduct(id);
    }
    @PostMapping("/v3/product/add-product")
    public @ResponseBody
    Object addNewProduct(@RequestBody Product product) {
        return productService.addNewProduct(product);
    }
    @PutMapping("/v3/product/update-product")
    public @ResponseBody
    Object updateExistProduct(@RequestBody Product product) {
        return productService.updateExistProduct(product);
    }
}
