package com.example.Java_BE.controller;

import com.example.Java_BE.model.Order;
import com.example.Java_BE.payload.request.PaymentReq;
import com.example.Java_BE.payload.request.UpdateStatusOrderReq;
import com.example.Java_BE.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@Controller
@RequestMapping("/api")
public class OrderController {
    @Autowired
    private OrderService orderService;

    //user
    @GetMapping("/v2/order")
    public @ResponseBody
    Object getAllOrder() {
        return orderService.getAllOrder();
    }

    @GetMapping("/v2/order/{id}")
    public @ResponseBody
    Object getOrderByID(@PathVariable String id) {
        return orderService.getOrderByID(id);
    }

    @DeleteMapping("/v2/order/{id}")
    public @ResponseBody
    Object deleteOrderByID(@PathVariable String id) {
        return orderService.deleteOrderByID(id);
    }

    @PostMapping("/v2/order/add-order")
    public @ResponseBody
    Object addOrder(@RequestBody Order order) {
        return orderService.addOrder(order);
    }

    @PostMapping("/v2/order/buy-now")
    public @ResponseBody
    Object addOrderImmediately(@RequestBody Order order) {
        return orderService.addOrderImmediately(order);
    }

    @PutMapping("/v2/order/update-order")
    public @ResponseBody
    Object updateOrder(@RequestBody Order order) {
        return orderService.updateOrder(order);
    }

    @PutMapping("/v2/order/update-payment")
    public @ResponseBody
    Object addPaymentMethod(@RequestBody PaymentReq paymentReq) {
        return orderService.addPaymentMethod(paymentReq);
    }

    @GetMapping("/v2/order/complete")
    public @ResponseBody
    Object getCompleteOrder() {
        return orderService.getCompleteOrder();
    }

    @GetMapping("/v2/order/pending")
    public @ResponseBody
    Object getPendingOrder() {
        return orderService.getPendingOrder();
    }

    @GetMapping("/v2/order/cancel")
    public @ResponseBody
    Object getCancelOrder() {
        return orderService.getCancelOrder();
    }

    @GetMapping("/v2/order/detail")
    public @ResponseBody
    Object getAllOrderDetail() {
        return orderService.getOrderDetail();
    }

    @GetMapping("/v2/order/detail/{id}")
    public @ResponseBody
    Object getOrderDetailByID(@PathVariable String id) {
        return orderService.getOrderDetailById(id);
    }

    @GetMapping("/v2/order/delivery")
    public @ResponseBody
    Object getDeliveryOrder() {
        return orderService.getDeliveryOrder();
    }

    //staff

    @PutMapping("/v3/order/update-status")
    public @ResponseBody
    Object updateStatusOrder(@RequestBody UpdateStatusOrderReq req) throws MessagingException {
        return orderService.updateStatusOrder(req);
    }
    @GetMapping("/v3/order/get-all-order")
    public  @ResponseBody
    Object getAllOrderByStaff() {
        return  orderService.getAllOrderByStaff();
    }
    //admin
}
