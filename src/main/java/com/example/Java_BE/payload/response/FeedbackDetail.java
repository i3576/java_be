package com.example.Java_BE.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackDetail {
    private GetUserInfoRes user;
    private float rateStar;
    private String content;
    private Date createAt;
    private Object sentimentResult;
}
