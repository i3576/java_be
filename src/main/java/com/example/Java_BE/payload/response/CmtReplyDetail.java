package com.example.Java_BE.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CmtReplyDetail {
    private String _id;
    private String ownerId; //này phải là quản trị viên mới đc
    private String content;
    private String productCommentId;
    private Date createdAt;
    private Date updatedAt;

    private GetUserInfoRes user;
}
