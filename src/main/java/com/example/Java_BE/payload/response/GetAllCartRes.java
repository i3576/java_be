package com.example.Java_BE.payload.response;

import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;

public class GetAllCartRes {
    private Product product;
    private Specification specification;
    private int quantity;
    private String id;

    public GetAllCartRes() {
    }

    public GetAllCartRes(Product product, Specification specification, int quantity, String id) {
        this.product = product;
        this.specification = specification;
        this.quantity = quantity;
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Specification getSpecification() {
        return specification;
    }

    public void setSpecification(Specification specification) {
        this.specification = specification;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
