package com.example.Java_BE.payload.response;

import com.example.Java_BE.model.Cart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {
    private String _id;
    private String userId;
    private List<CartDetail> carts = new ArrayList<>();
    private float price;
    private int deliveryFees;

    private String fullName;
    private String phone;
    private String address;

    private int orderStatus;
    private String cancelReason;

    private String paymentMethod;
    private int paymentStatus;

    private Date createAt;
    private Date updateAt;
}
