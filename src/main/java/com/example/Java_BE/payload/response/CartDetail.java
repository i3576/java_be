package com.example.Java_BE.payload.response;

import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDetail {
    private Specification specification;
    private Product product;
    private int quantity;
}
