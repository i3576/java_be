package com.example.Java_BE.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackDetailAdmin {
    private GetUserInfoRes user;
    private String productName;
    private float rateStar;
    private String content;
    private Date createAt;
}
