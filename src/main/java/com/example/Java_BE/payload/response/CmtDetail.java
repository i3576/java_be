package com.example.Java_BE.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CmtDetail {
    private String _id;
    private String ownerId;
    private String content;
    private String productId;
    private Date createdAt;
    private Date updatedAt;

    private GetUserInfoRes user;

    private CmtReplyDetail cmtReplyDetail;
}
