package com.example.Java_BE.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCommentStaffRes {
    private String userName;
    private String productName;
    private String content;
    private String commentId;
}
