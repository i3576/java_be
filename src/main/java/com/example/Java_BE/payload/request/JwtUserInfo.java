package com.example.Java_BE.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtUserInfo {
    private String username;
    private String role;
    public void JwtUserInfo(String _username, String _role) {
        username = _username;
        role = _role;
    }
}
