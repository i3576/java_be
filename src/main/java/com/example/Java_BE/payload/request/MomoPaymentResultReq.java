package com.example.Java_BE.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MomoPaymentResultReq {
    private String signature;
    private String extraData;
    private String responseTime;
    private String payType;
    private String message;
    private int resultCode;
    private String transId;
    private String orderType;
    private String orderInfo;
    private String amount;
    private String requestId;
    private String orderId;
    private String partnerCode;
}
