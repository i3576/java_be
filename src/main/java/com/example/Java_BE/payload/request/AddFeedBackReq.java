package com.example.Java_BE.payload.request;

import com.example.Java_BE.model.Rate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddFeedBackReq {
    private String productId;
    private String content;
    private float rateStar;
}
