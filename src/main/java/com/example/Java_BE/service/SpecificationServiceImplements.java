package com.example.Java_BE.service;

import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;
import com.example.Java_BE.payload.response.SpecificationDetailRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class SpecificationServiceImplements implements SpecificationService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Object getSpecificationById(String id) {
        Specification exist = mongoTemplate.findById(id, Specification.class);
        if (exist == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Không tồn tại mô tả chi tiết!");
            return map;
        } else {
            return exist;
        }
    }

    @Override
    public Object addSpecification(Specification specification) {
        specification.setCreateAt(new Date());
        specification.setUpdateAt(new Date());
        mongoTemplate.save(specification);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object updateSpecification(Specification specification) {
        Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(specification.get_id()));
        Specification spe = mongoTemplate.findOne(q, Specification.class);
        if (spe == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Không tồn tại mô tả chi tiết!");
            return map;
        } else {
            specification.setUpdateAt(new Date());
            mongoTemplate.save(specification);
            return Collections.singletonMap("statusCode", 201);
        }
    }

    @Override
    public Object deleteSpecification(String id) {
        Specification exist = mongoTemplate.findById(id, Specification.class);
        if (exist == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Không tồn tại mô tả chi tiết!");
            return map;
        } else {
            mongoTemplate.remove(exist);
            return Collections.singletonMap("statusCode", 201);
        }
    }

    @Override
    public Object getAllSpecification() {
        List<SpecificationDetailRes> lstRes = new ArrayList<>();

        List<Specification> lstSpe = mongoTemplate.findAll(Specification.class);

        for (Specification item : lstSpe) {
            Product product = mongoTemplate.findById(item.getProductId(), Product.class);
            lstRes.add(new SpecificationDetailRes(product, item));
        }

        return lstRes;
    }
}
