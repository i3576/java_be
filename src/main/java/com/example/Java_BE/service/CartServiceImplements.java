package com.example.Java_BE.service;

import com.example.Java_BE.model.Cart;
import com.example.Java_BE.model.CustomUser;
import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;
import com.example.Java_BE.payload.response.GetAllCartRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Repository
public class CartServiceImplements implements CartService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Object addCart(Cart cart) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        cart.setUserId(userInfo.get_id());

        Specification spe = mongoTemplate.findById(cart.getSpecificationId(), Specification.class);
        if (spe.getQuantity() < cart.getQuantity() || cart.getQuantity() <= 0) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Số lượng bạn muốn đặt không hợp lệ");
            return map;
        } else {
            mongoTemplate.save(cart);
            return Collections.singletonMap("statusCode", 201);
        }
    }

    @Override
    public Object updateCart(Cart cart) {
        Cart cartTemp = mongoTemplate.findById(cart.get_id(), Cart.class);
        if (cartTemp != null) {
            Specification spe = mongoTemplate.findById(cart.getSpecificationId(), Specification.class);
            if (spe.getQuantity() < cart.getQuantity() || cart.getQuantity() <= 0) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("statusCode", 400);
                map.put("errorMess", "Số lượng bạn muốn đặt không hợp lệ");
                return map;
            } else {
                cartTemp.setQuantity(cart.getQuantity());
                mongoTemplate.save(cartTemp);
                return Collections.singletonMap("statusCode", 201);
            }
        } else {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Đơn của bạn không còn tồn tại");
            return map;
        }
    }

    @Override
    public Object getAllCart() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);
        Query q2 = new Query();
        q2.addCriteria(Criteria.where("userId").is(userInfo.get_id()));
        List<Cart> listCart = mongoTemplate.find(q2, Cart.class);
        List<GetAllCartRes> listAllCart = new ArrayList<>();
        for (int i = 0; i < listCart.size(); i++) {
            Specification spec = mongoTemplate.findById(listCart.get(i).getSpecificationId(), Specification.class);
            Product pro = mongoTemplate.findById(spec.getProductId(), Product.class);
            listAllCart.add(new GetAllCartRes(pro, spec, listCart.get(i).getQuantity(), listCart.get(i).get_id()));
        }
        return listAllCart;
    }

    @Override
    public Object getAllCartInfo() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);
        Query q2 = new Query();
        q2.addCriteria(Criteria.where("userId").is(userInfo.get_id()));
        return mongoTemplate.find(q2, Cart.class);

    }

    @Override
    public Object deleteCart(String cartId) {
        Cart cart = mongoTemplate.findById(cartId, Cart.class);

        if (cart == null) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Không tồn tại phần tử này trong giỏ hàng");
            return map;
        } else {

            mongoTemplate.remove(cart);
            return Collections.singletonMap("statusCode", 201);
        }
    }
}
