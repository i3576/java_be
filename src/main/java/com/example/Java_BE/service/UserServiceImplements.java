package com.example.Java_BE.service;

import com.example.Java_BE.model.CustomUser;
import com.example.Java_BE.payload.request.ChangePasswordReq;
import com.example.Java_BE.payload.response.GetUserInfoRes;
import com.example.Java_BE.payload.request.JwtUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserServiceImplements implements UserDetailsService {

    @Autowired
    MongoTemplate mongoTemplate;

    public Object getUserByName(String name){
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(name));

        return mongoTemplate.findOne(q,CustomUser.class);
    }

    public Object getInfo(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);
        return new GetUserInfoRes(userInfo.getFullName(), userInfo.getPhone(), userInfo.getAddress(), userInfo.getEmail(), userInfo.get_id());
    }

    public boolean isExistUserName(String name){
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(name));

        return mongoTemplate.findOne(q,CustomUser.class)!=null;
    }

    public boolean isExistEmail(String email){
        Query q = new Query();
        q.addCriteria(Criteria.where("email").is(email));

        return mongoTemplate.findOne(q,CustomUser.class)!=null;
    }

    public Object updateUserInfo(CustomUser user){
        User userJwt = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(userJwt.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        userInfo.setFullName(user.getFullName());
        userInfo.setAddress(user.getAddress());
        userInfo.setPhone(user.getPhone());

        mongoTemplate.save(userInfo);

        return Collections.singletonMap("statusCode", 201);
    }

    public Object getAllStaff()
    {
        Query q = new Query();
        q.addCriteria(Criteria.where("role").is("ROLE_STAFF"));
        return mongoTemplate.find(q,CustomUser.class);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(userName));

        CustomUser user = mongoTemplate.findOne(q,CustomUser.class);

        return new User(user.getUsername(),user.getPassword(), Collections.singleton(new SimpleGrantedAuthority(user.getRole())));
    }

    public JwtUserInfo getUserByUsername(String userName) throws UsernameNotFoundException {

        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(userName));

        CustomUser user = mongoTemplate.findOne(q,CustomUser.class);

        return new JwtUserInfo(user.getUsername(), user.getRole());
    }

}
