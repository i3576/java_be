package com.example.Java_BE.service;

import com.example.Java_BE.model.*;
import com.example.Java_BE.payload.request.PaymentReq;
import com.example.Java_BE.payload.request.UpdateStatusOrderReq;
import com.example.Java_BE.payload.response.CartDetail;
import com.example.Java_BE.payload.response.OrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Repository;

import javax.mail.MessagingException;
import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class OrderServiceImplements implements OrderService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    EmailSenderService emailSenderService;

    @Override
    public Object addOrder(Order order) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);
        for (Cart item : order.getCarts()) {
            if (!userInfo.get_id().equals(item.getUserId())) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("statusCode", 400);
                map.put("errorMess", "Bạn không có quyền");
                return map;
            }
            Specification spe = mongoTemplate.findById(item.getSpecificationId(), Specification.class);
            if (spe.getQuantity() < item.getQuantity()) {
                Product product = mongoTemplate.findById(spe.getProductId(), Product.class);
                HashMap<String, Object> map = new HashMap<>();
                map.put("statusCode", 400);
                map.put("errorMess", "Số lượng sản phẩm " + product.getName() + " bạn muốn đặt không đủ để cung cấp");
                return map;
            }
        }

        int price = 0;
        int deliveryFees = 30000;

        for (Cart item : order.getCarts()) {
            Specification spe = mongoTemplate.findById(item.getSpecificationId(), Specification.class);
            Cart cart = mongoTemplate.findById(item.get_id(), Cart.class);
            Product product = mongoTemplate.findById(spe.getProductId(), Product.class);

            spe.setQuantity(spe.getQuantity() - item.getQuantity());
            price += (1 - product.getDiscount()) * item.getQuantity() * spe.getPrice();

            mongoTemplate.save(spe);

            mongoTemplate.remove(cart);
        }

        price += deliveryFees;

        order.setPrice(price);
        order.setDeliveryFees(deliveryFees);

        order.setUserId(userInfo.get_id());
        order.setFullName(userInfo.getFullName());
        order.setAddress(userInfo.getAddress());

        order.setOrderStatus(1);
        order.setCreateAt(new Date());
        order.setUpdateAt(new Date());

        Order orderSaved = mongoTemplate.save(order);

        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", 201);
        map.put("orderId", orderSaved.get_id());
        return map;
    }

    @Override
    public Object addOrderImmediately(Order order) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);
        Specification spe = mongoTemplate.findById(order.getCarts().get(0).getSpecificationId(), Specification.class);
        Product product = mongoTemplate.findById(spe.getProductId(), Product.class);

        if (spe.getQuantity() < order.getCarts().get(0).getQuantity()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Số lượng sản phẩm " + product.getName() + " bạn muốn đặt không đủ để cung cấp");
            return map;
        } else {
            Query q2 = new Query();
            q.addCriteria(Criteria.where("userId").is(userInfo.get_id()));
            List<Cart> lstCart = mongoTemplate.find(q2, Cart.class);

            for (Cart item : lstCart) {
                mongoTemplate.remove(item);
            }

            order.setPrice((1 - product.getDiscount()) * (spe.getPrice() * order.getCarts().get(0).getQuantity()));
            order.setDeliveryFees(30000);

            order.setUserId(userInfo.get_id());
            order.setFullName(order.getFullName());
            order.setAddress(order.getAddress());

            order.setOrderStatus(1);
            order.setCreateAt(new Date());
            order.setUpdateAt(new Date());

            spe.setQuantity(spe.getQuantity() - order.getCarts().get(0).getQuantity());

            mongoTemplate.save(spe);

            Order orderSaved = mongoTemplate.save(order);

            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 201);
            map.put("orderId", orderSaved.get_id());
            return map;
        }
    }

    @Override
    public Object updateOrder(Order order) {
        Order orderTemp = mongoTemplate.findById(order.get_id(), Order.class);

        orderTemp.setFullName(order.getFullName());
        orderTemp.setAddress(order.getAddress());
        orderTemp.setPhone(order.getPhone());

        orderTemp.setUpdateAt(new Date());

        mongoTemplate.save(orderTemp);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object addPaymentMethod(PaymentReq paymentReq) {
        Order order = mongoTemplate.findById(paymentReq.getIdOrder(), Order.class);

        order.setPaymentMethod(paymentReq.getMethod());
        order.setPaymentStatus(paymentReq.getStatus());

        order.setUpdateAt(new Date());

        mongoTemplate.save(order);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object getOrderByID(String Id) {
        return mongoTemplate.findById(Id, Order.class);
    }

    @Override
    public Object deleteOrderByID(String Id) {
        Order orderTemp = mongoTemplate.findById(Id, Order.class);
        mongoTemplate.remove(orderTemp);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object getAllOrder() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        Query q2 = new Query();
        q2.addCriteria(Criteria.where("userId").is(userInfo.get_id()));
        return mongoTemplate.find(q2, Order.class);
    }

    @Override
    public Object getCompleteOrder() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        Query q2 = new Query();
        q2.addCriteria(
                new Criteria().andOperator(
                        Criteria.where("userId").is(userInfo.get_id()),
                        Criteria.where("status").is(1)
                )
        );

        List<Order> completeOrderList = mongoTemplate.find(q2, Order.class);
        return completeOrderList;
    }

    @Override
    public Object getCancelOrder() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        Query q2 = new Query();
        q2.addCriteria(
                new Criteria().andOperator(
                        Criteria.where("userId").is(userInfo.get_id()),
                        Criteria.where("status").is(0)
                )
        );

        List<Order> cancelOrderList = mongoTemplate.find(q2, Order.class);
        return cancelOrderList;
    }

    @Override
    public Object getPendingOrder() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        Query q2 = new Query();
        q2.addCriteria(
                new Criteria().andOperator(
                        Criteria.where("userId").is(userInfo.get_id()),
                        Criteria.where("status").is(1)
                )
        );
        List<Order> pendingOrderList = mongoTemplate.find(q2, Order.class);
        return pendingOrderList;
    }

    @Override
    public Object getDeliveryOrder() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        Query q2 = new Query();
        q2.addCriteria(
                new Criteria().andOperator(
                        Criteria.where("userId").is(userInfo.get_id()),
                        Criteria.where("status").is(2)
                )
        );
        List<Order> deliveryOrderList = mongoTemplate.find(q2, Order.class);
        return deliveryOrderList;
    }

    @Override
    public Object updateStatusOrder(UpdateStatusOrderReq req) throws MessagingException {
        Order order = mongoTemplate.findById(req.getOrderId(), Order.class);
        order.setOrderStatus(req.getStatus());
        order.setUpdateAt(new Date());
        CustomUser user = mongoTemplate.findById(order.getUserId(), CustomUser.class);

        if (req.getStatus() == 0) {
            order.setCancelReason(req.getCancelReason());
        }

        Order temp = mongoTemplate.save(order);

        if (temp.getOrderStatus() == 0) {
            notifyStatusOrder(user.getEmail(), temp, true);
        }

        if (temp.getOrderStatus() == 2) {
            notifyStatusOrder(user.getEmail(), temp, false);
        }

        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object getOrderDetailById(String id) {
        Order order = mongoTemplate.findById(id, Order.class);

        List<CartDetail> carts = new ArrayList<>();

        for (Cart item : order.getCarts()) {
            Specification spe = mongoTemplate.findById(item.getSpecificationId(), Specification.class);
            Product product = mongoTemplate.findById(spe.getProductId(), Product.class);

            carts.add(new CartDetail(spe, product, item.getQuantity()));
        }

        return new OrderDetail(order.get_id(), order.getUserId(), carts, order.getPrice(), order.getDeliveryFees(), order.getFullName(), order.getPhone(), order.getAddress(), order.getOrderStatus(), order.getCancelReason(), order.getPaymentMethod(), order.getPaymentStatus(), order.getCreateAt(), order.getUpdateAt());
    }

    @Override
    public Object getOrderDetail() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        Query q2 = new Query();
        q2.addCriteria(Criteria.where("userId").is(userInfo.get_id()));

        List<Order> lstOrder = mongoTemplate.find(q2, Order.class);

        List<Object> lstDetail = new ArrayList<>();
        for (Order item : lstOrder) {
            lstDetail.add(getOrderDetailById(item.get_id()));
        }
        return lstDetail;
    }

    @Override
    public Object getAllOrderByStaff() {
        List<OrderDetail> orderDetailLst = new ArrayList<>();
        List<Order> orderLst = mongoTemplate.findAll(Order.class);
        for (Order order : orderLst) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.set_id(order.get_id());
            orderDetail.setUserId(order.get_id());
            orderDetail.setPrice(order.getPrice());
            orderDetail.setDeliveryFees(order.getDeliveryFees());
            orderDetail.setFullName(order.getFullName());
            orderDetail.setAddress(order.getAddress());
            orderDetail.setPhone(order.getPhone());
            orderDetail.setOrderStatus(order.getOrderStatus());
            orderDetail.setCancelReason(order.getCancelReason());
            orderDetail.setPaymentMethod(order.getPaymentMethod());
            orderDetail.setPaymentStatus(order.getPaymentStatus());
            orderDetail.setCreateAt(order.getCreateAt());
            orderDetail.setUpdateAt(order.getUpdateAt());

            List<CartDetail> carts = new ArrayList<>();
            for (Cart item : order.getCarts()) {
                Specification spe = mongoTemplate.findById(item.getSpecificationId(), Specification.class);
                Product product = mongoTemplate.findById(spe.getProductId(), Product.class);

                carts.add(new CartDetail(spe, product, item.getQuantity()));
            }
            orderDetail.setCarts(carts);

            orderDetailLst.add(orderDetail);
        }
        return orderDetailLst;
    }

    public void notifyStatusOrder(String mailUser, Order order, boolean isCancel) throws MessagingException {

        Mail email = new Mail();
        email.setTo(mailUser);
        email.setFrom("SellPhone");
        email.setSubject("Thông báo trạng thái đơn hàng");
        email.setContent("Sending mail");

        Map<String, Object> model = new HashMap<>();

        SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");

        if (isCancel == true) {
            model.put("title", "THÔNG BÁO HỦY ĐƠN HÀNG");
            model.put("fullname", order.getFullName());
            model.put("mess", "Chúng tôi rất tiếc phải thông báo với bạn rằng đơn hàng của bạn đã bị hủy vì lí do: <b>" + order.getCancelReason() + "</b>");
            model.put("orderId", order.get_id());
            model.put("createAt", sm.format(order.getCreateAt()));
            model.put("price", order.getPrice());
            model.put("name", order.getFullName());
            model.put("phone", order.getPhone());
            model.put("address", order.getAddress());
            email.setModel(model);
        } else {
            model.put("title", "XÁC NHẬN ĐƠN HÀNG THÀNH CÔNG");
            model.put("fullname", order.getFullName());
            model.put("mess", "Chúng tôi xin được thông báo với bạn rằng đơn hàng của bạn đã được xác nhận");
            model.put("orderId", order.get_id());
            model.put("createAt", sm.format(order.getCreateAt()));
            model.put("price", order.getPrice());
            model.put("name", order.getFullName());
            model.put("phone", order.getPhone());
            model.put("address", order.getAddress());
            email.setModel(model);
        }
        emailSenderService.sendEmailWithTemplate(email, "/order-status.flth");

    }
}