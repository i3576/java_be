package com.example.Java_BE.service;

import com.example.Java_BE.payload.request.AddFeedBackReq;

import java.util.List;

public interface FeedbackService {
    public Object addFeedback(List<AddFeedBackReq> req, String id);

    public Object getAllFeedbackOfProduct(String id);

    public Object getAllFeedbackForAdmin();
}
