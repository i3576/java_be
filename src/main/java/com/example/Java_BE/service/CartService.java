package com.example.Java_BE.service;

import com.example.Java_BE.model.Cart;
import org.springframework.security.oauth2.core.user.OAuth2User;

public interface CartService {
    Object addCart(Cart cart);
    Object updateCart(Cart cart);
    Object getAllCart();
    Object deleteCart(String cartId);
    Object getAllCartInfo();
}
