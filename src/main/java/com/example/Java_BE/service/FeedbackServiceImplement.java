package com.example.Java_BE.service;

import com.example.Java_BE.controller.BaseController;
import com.example.Java_BE.model.CustomUser;
import com.example.Java_BE.model.Order;
import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Rate;
import com.example.Java_BE.payload.request.AddFeedBackReq;
import com.example.Java_BE.payload.response.FeedbackDetail;
import com.example.Java_BE.payload.response.FeedbackDetailAdmin;
import com.example.Java_BE.payload.response.GetUserInfoRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class FeedbackServiceImplement implements FeedbackService {
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    private BaseController baseController;

    @Override
    public Object addFeedback(List<AddFeedBackReq> req, String orderId) {
        Order order = mongoTemplate.findById(orderId, Order.class);
        order.setRating(true);
        mongoTemplate.save(order);

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        for (AddFeedBackReq item : req) {
            Query q2 = new Query();
            q2.addCriteria(Criteria.where("productId").is(item.getProductId()));
            List<Rate> lstRate = mongoTemplate.find(q2, Rate.class);

            Product product = mongoTemplate.findById(item.getProductId(), Product.class);

            if (lstRate.size() == 0) {
                product.setRating(item.getRateStar());
                mongoTemplate.save(product);
            } else {
                float ratingNow = (product.getRating() * lstRate.size() + item.getRateStar()) / (lstRate.size() + 1);
                product.setRating(ratingNow);
                mongoTemplate.save(product);
            }
            Object sentiment = baseController.analiseSentiment(item.getContent());
            mongoTemplate.save(new Rate(userInfo.get_id(), item.getProductId(), item.getRateStar(), item.getContent(), new Date(), sentiment));
        }
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object getAllFeedbackOfProduct(String id) {
        List<FeedbackDetail> FeedbackDetailList = new ArrayList<>();

        Query q = new Query();
        q.addCriteria(Criteria.where("productId").is(id));
        List<Rate> RateList = mongoTemplate.find(q, Rate.class);

        if (!RateList.isEmpty()) {
            for (Rate rate : RateList) {
                FeedbackDetail fb = new FeedbackDetail();

                fb.setRateStar(rate.getRateStar());
                fb.setContent(rate.getContent());
                fb.setCreateAt(rate.getCreateAt());
                fb.setSentimentResult(rate.getSentimentRerult());

                CustomUser customUser = mongoTemplate.findById(rate.getUserId(), CustomUser.class);
                fb.setUser(new GetUserInfoRes(customUser.getFullName(), customUser.getPhone(), customUser.getAddress(), customUser.getEmail(), customUser.get_id()));

                FeedbackDetailList.add(fb);
            }
        }
        return FeedbackDetailList;
    }

    @Override
    public Object getAllFeedbackForAdmin() {
        List<FeedbackDetailAdmin> FeedbackList = new ArrayList<>();

        List<Rate> RateList = mongoTemplate.findAll(Rate.class);

        if (!RateList.isEmpty()) {
            for (Rate rate : RateList) {
                FeedbackDetailAdmin fb = new FeedbackDetailAdmin();

                Product product = mongoTemplate.findById(rate.getProductId(), Product.class);
                fb.setProductName(product.getName());

                fb.setRateStar(rate.getRateStar());
                fb.setContent(rate.getContent());
                fb.setCreateAt(rate.getCreateAt());

                CustomUser customUser = mongoTemplate.findById(rate.getUserId(), CustomUser.class);
                fb.setUser(new GetUserInfoRes(customUser.getFullName(), customUser.getPhone(), customUser.getAddress(), customUser.getEmail(), customUser.get_id()));

                FeedbackList.add(fb);
            }
        }
        return FeedbackList;
    }
}
