package com.example.Java_BE.service;

import com.example.Java_BE.payload.request.MomoPaymentReq;
import com.example.Java_BE.payload.request.MomoPaymentResultReq;

public interface MomoService {
    Object createPayment(MomoPaymentReq req);
    Object paymentResult (MomoPaymentResultReq req);
}
