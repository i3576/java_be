package com.example.Java_BE.service;

import com.example.Java_BE.model.CustomUser;
import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.ProductComment;
import com.example.Java_BE.model.ReplyComment;
import com.example.Java_BE.payload.response.CmtDetail;
import com.example.Java_BE.payload.response.CmtReplyDetail;
import com.example.Java_BE.payload.response.GetCommentStaffRes;
import com.example.Java_BE.payload.response.GetUserInfoRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class CommentServiceImplement implements CommentService {
    @Autowired
    MongoTemplate mongoTemplate;

    public Object getAllComment(String productId) {
        Query q = new Query();
        q.addCriteria(Criteria.where("productId").is(productId));

        List<CmtDetail> lstCmtDetail = new ArrayList<>();

        List<ProductComment> lstPCmt = mongoTemplate.find(q, ProductComment.class);

        for (ProductComment item : lstPCmt) {
            CustomUser customUserCmt = mongoTemplate.findById(item.getOwnerId(), CustomUser.class);
            GetUserInfoRes userCmt = new GetUserInfoRes(customUserCmt.getFullName(), customUserCmt.getPhone(), customUserCmt.getAddress(), customUserCmt.getEmail(), customUserCmt.get_id());

            if (item.getReply() == null) {
                lstCmtDetail.add(new CmtDetail(item.get_id(), item.getOwnerId(), item.getContent(), item.getProductId(), item.getCreatedAt(), item.getUpdatedAt(), userCmt, null));
            } else {
                CustomUser customUserReplay = mongoTemplate.findById(item.getReply().getOwnerId(), CustomUser.class);
                GetUserInfoRes userReplay = new GetUserInfoRes(customUserReplay.getFullName(), customUserReplay.getPhone(), customUserReplay.getAddress(), customUserReplay.getEmail(), customUserCmt.get_id());

                CmtReplyDetail cmtReplyDetail = new CmtReplyDetail(item.getReply().get_id(), item.getReply().getOwnerId(), item.getReply().getContent(), item.getReply().getProductCommentId(), item.getReply().getCreatedAt(), item.getReply().getUpdatedAt(), userReplay);

                lstCmtDetail.add(new CmtDetail(item.get_id(), item.getOwnerId(), item.getContent(), item.getProductId(), item.getCreatedAt(), item.getUpdatedAt(), userCmt, cmtReplyDetail));
            }
        }
        Collections.reverse(lstCmtDetail);
        return lstCmtDetail;
    }

    @Override
    public void createComment(ProductComment cmt) {
        cmt.setCreatedAt(new Date());
        cmt.setUpdatedAt(new Date());
        cmt.setReply(null);
        mongoTemplate.save(cmt);
    }

    @Override
    public void deleteComment(ProductComment cmt) {
        ProductComment cmtDelete = mongoTemplate.findById(cmt.get_id(), ProductComment.class);
        mongoTemplate.remove(cmtDelete);
    }

    @Override
    public void createCommentReply(ReplyComment cmt) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Query q = new Query();
        q.addCriteria(Criteria.where("username").is(user.getUsername()));
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

        cmt.setOwnerId(userInfo.get_id());
        cmt.setCreatedAt(new Date());
        cmt.setUpdatedAt(new Date());

        ProductComment productComment = mongoTemplate.findById(cmt.getProductCommentId(),ProductComment.class);

        productComment.setReply(cmt);

        mongoTemplate.save(productComment);
    }

    @Override
    public Object getAllCommentStaff() {
        List<GetCommentStaffRes> lstRes = new ArrayList<>();
        Query q = new Query();
        q.addCriteria(Criteria.where("reply").exists(false));
        List<ProductComment> lstPCmt = mongoTemplate.find(q, ProductComment.class);

        for (ProductComment item : lstPCmt) {
//            if (item.getReply() != null) {
                CustomUser user = mongoTemplate.findById(item.getOwnerId(), CustomUser.class);
                Product product = mongoTemplate.findById(item.getProductId(), Product.class);

                lstRes.add(new GetCommentStaffRes(user.getFullName(), product.getName(), item.getContent(), item.get_id()));
//            }
        }

        return lstRes;
    }
}
