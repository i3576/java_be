package com.example.Java_BE.service;

import com.example.Java_BE.model.Order;
import com.example.Java_BE.payload.request.PaymentReq;
import com.example.Java_BE.payload.request.UpdateStatusOrderReq;
import org.springframework.security.oauth2.core.user.OAuth2User;

import javax.mail.MessagingException;

public interface OrderService {
    Object addOrder(Order order);
    Object addOrderImmediately(Order order);
    Object updateOrder(Order order);
    Object addPaymentMethod(PaymentReq paymentReq);
    Object getOrderByID(String Id);
    Object deleteOrderByID(String Id);
    Object getAllOrder();
    Object getCompleteOrder();
    Object getCancelOrder();
    Object getPendingOrder();
    Object getDeliveryOrder();
    Object updateStatusOrder(UpdateStatusOrderReq req) throws MessagingException;
    Object getOrderDetailById(String id);
    Object getOrderDetail();
    Object getAllOrderByStaff();
}