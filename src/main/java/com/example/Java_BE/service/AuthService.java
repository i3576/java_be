package com.example.Java_BE.service;

import com.example.Java_BE.payload.request.ChangePasswordReq;
import com.example.Java_BE.payload.request.ResetPasswordReq;

import javax.mail.MessagingException;

public interface AuthService {
    public Object updatePassword(ChangePasswordReq data);
    public Object resetPassword(ResetPasswordReq data) throws MessagingException;
}
