package com.example.Java_BE.service;

import com.example.Java_BE.model.Category;

public interface CategoryService {
    Object addCategory(Category category);
    Object getExistCategories();
}
