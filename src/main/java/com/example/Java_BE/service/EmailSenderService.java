package com.example.Java_BE.service;

import com.example.Java_BE.model.Mail;
import com.example.Java_BE.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@EnableScheduling
public class EmailSenderService {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    public void sendEmailWithTemplate(Mail mail, String filePath) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setSubject(mail.getSubject());
            mimeMessageHelper.setFrom(mail.getFrom(),"SellPhone");
            mimeMessageHelper.setTo(mail.getTo());
            mail.setContent(getContentFromTemplate(mail.getModel(),filePath));
            mimeMessageHelper.setText(mail.getContent(), true);

            javaMailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String getContentFromTemplate(Map<String, Object> model,String filePath) {
        StringBuffer content = new StringBuffer();

        try {
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(freeMarkerConfigurer.getConfiguration().getTemplate(filePath), model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }

//    @Scheduled(cron = "0 0 0/1 1/1 * *")
//    public void mailForStaff() throws MessagingException {
//        Mail email = new Mail();
//        email.setTo("nguyenngockhuongduy172@gmail.com");
//        email.setFrom("SellPhone");
//        email.setSubject("Xác nhận đơn hàng còn tồn");
//        email.setContent("Sending mail");
//
//        Query q = new Query();
//        q.addCriteria(Criteria.where("status").is(1));
//        List<Order> orderList = mongoTemplate.find(q,Order.class);
//
//        if(orderList.size()==0)
//        {
//            return;
//        }
//
//        String text = "";
//        for (Order item:orderList)
//        {
//            text+="<p>"+item.get_id()+"</p>";
//        }
//
//        Map<String, Object> model = new HashMap<>();
//        model.put("id", text);
//        email.setModel(model);
//        sendEmailWithTemplate(email);
//        email.setTo("chuongbinhtran2001@gmail.com");
//        sendEmailWithTemplate(email);
//    }
}

