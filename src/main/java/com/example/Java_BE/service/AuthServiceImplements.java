package com.example.Java_BE.service;

import com.example.Java_BE.config.SecurityConfig;
import com.example.Java_BE.model.CustomUser;
import com.example.Java_BE.model.Mail;
import com.example.Java_BE.payload.request.ChangePasswordReq;
import com.example.Java_BE.payload.request.JwtUserInfo;
import com.example.Java_BE.payload.request.ResetPasswordReq;
import com.example.Java_BE.payload.response.JwtResponse;
import com.example.Java_BE.utility.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.mail.MessagingException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@Repository
public class AuthServiceImplements implements AuthService {
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    SecurityConfig securityConfig;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private UserServiceImplements userService;

    @Autowired
    private EmailSenderService emailSenderService;

    public Object updatePassword(ChangePasswordReq data) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (securityConfig.passwordEncoder().matches(data.getOldPassword(), user.getPassword())) {
            String ePassword = encoder.encode(data.getNewPassword());

            Query q = new Query();
            q.addCriteria(Criteria.where("username").is(user.getUsername()));
            CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);

            userInfo.setPassword(ePassword);

            mongoTemplate.save(userInfo);

            final JwtUserInfo userDetails
                    = userService.getUserByUsername(user.getUsername());

            final String token =
                    jwtUtility.generateToken(userDetails);

            return new JwtResponse(token);
        } else {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Mật khẩu cũ không trùng khớp");
            return map;
        }
    }

    @Override
    public Object resetPassword(ResetPasswordReq data) throws MessagingException {
        Query q = new Query();
        q.addCriteria(new Criteria("username").is(data.getUsername()).and("email").is(data.getEmail())
        );
        CustomUser userInfo = mongoTemplate.findOne(q, CustomUser.class);
        if (userInfo !=null ) {
            String newPassword = randomPassword(8);
            String newHashedPassword = encoder.encode(newPassword);
            userInfo.setPassword(newHashedPassword);
            mongoTemplate.save(userInfo);
            recoverPasswordEmail(data.getEmail(),newPassword,userInfo.getFullName());
            return Collections.singletonMap("statusCode", 201);
        }else {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", 400);
            map.put("errorMess", "Không tồn tại người dùng!");
            return map;
        }
    }

    public void recoverPasswordEmail(String mailUser, String newPassword,String fullname) throws MessagingException {

            Mail email = new Mail();
            email.setTo(mailUser);
            email.setFrom("SellPhone");
            email.setSubject("Cấp mật khẩu mới");
            email.setContent("Sending mail");

            Map<String, Object> model = new HashMap<>();
            model.put("newPW", newPassword);
            model.put("fullname", fullname);
            email.setModel(model);
            emailSenderService.sendEmailWithTemplate(email,"/reset-password.flth");

    }

    String randomPassword(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }
}
