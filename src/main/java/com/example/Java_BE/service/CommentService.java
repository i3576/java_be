package com.example.Java_BE.service;

import com.example.Java_BE.model.ProductComment;
import com.example.Java_BE.model.ReplyComment;

public interface CommentService {
    public Object getAllComment (String productId);
    public Object getAllCommentStaff ();
    public void createComment (ProductComment cmt);
    public void deleteComment (ProductComment cmt);
    public void createCommentReply (ReplyComment cmt);
}
