package com.example.Java_BE.service;

import com.example.Java_BE.model.Specification;

public interface SpecificationService {
    Object getSpecificationById(String id);

    Object addSpecification(Specification specification);

    Object updateSpecification(Specification specification);

    Object deleteSpecification(String id);

    Object getAllSpecification();
}
