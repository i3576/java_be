package com.example.Java_BE.service;

import com.example.Java_BE.payload.request.MomoPaymentReq;
import com.example.Java_BE.payload.request.MomoPaymentResultReq;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Collections;

@Repository
public class MomoServiceImplement implements MomoService{
    @Override
    public Object createPayment(MomoPaymentReq req) {
        String uri = "https://test-payment.momo.vn/v2/gateway/api/create";
        String requestId = String.valueOf(System.currentTimeMillis());
        String rawSignature = "accessKey=" +
                "GwOJGeOJhaN4H4u5" +
                "&amount=" +
                req.getAmount() +
                "&extraData=" +
                "" +
                "&ipnUrl=" +
                "http://localhost:8102/api/v1/payment-result" +
                "&orderId=" +
                req.getOrderId() +
                "&orderInfo=" +
                "Payment" +
                "&partnerCode=" +
                "MOMOGSG620220517" +
                "&redirectUrl=" +
                "http://localhost:3000/payment/" +
                "&requestId=" +
                req.getOrderId() +
                "&requestType=" +
                "captureWallet";;
        String signature = null;
        try {
            signature = encode("amDCa7AnGuvSxpW2ZVgW05atKFmXGFpf", rawSignature);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // create request body
        JSONObject request = new JSONObject();
        request.put("partnerCode", "MOMOGSG620220517");
        request.put("accessKey", "GwOJGeOJhaN4H4u5");
        request.put("requestId", req.getOrderId());
        request.put("amount", req.getAmount());
        request.put("orderId", req.getOrderId());
        request.put("orderInfo", "Payment");
        request.put("redirectUrl", "http://localhost:3000/payment/");
        request.put("ipnUrl", "http://localhost:8102/api/v1/payment-result");
        request.put("extraData", "");
        request.put("requestType", "captureWallet");
        request.put("signature", signature);
        request.put("lang", "vi");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.postForObject(uri, entity, Object.class);
        return result;
    }
    @Override
    public Object paymentResult(MomoPaymentResultReq req) {
//        xử lý giao dịch thành công hay thất bại ở đây
        if(req.getResultCode() == 0) {
//            giao dịch thành công
            System.out.println("giao dịch thành công");
        } else {
//            giao dịch thất bại
        }
        return Collections.singletonMap("statusCode", 204);
    }
    private String encode(String key, String data) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        sha256_HMAC.init(secret_key);

        return Hex.encodeHexString(sha256_HMAC.doFinal(data.getBytes("UTF-8")));
    }

    private class ReqBody {
        String partnerCode;
        String accessKey;
        String requestId;
        long amount;
        String orderId;
        String orderInfo;
        String redirectUrl;
        String ipnUrl;
        String extraData;
        String requestType;
        String signature;
        String lang;
        public ReqBody(long _amount, String _signature, String _requestId){
            partnerCode = "MOMOGSG620220517";
            accessKey = "GwOJGeOJhaN4H4u5";
            requestId = _requestId;
            amount = _amount;
            orderId = _requestId;
            orderInfo = "Payment";
            redirectUrl = "https://dce2021.ml";
            ipnUrl = "https://callback.url/notify";
            extraData = "";
            requestType = "captureWallet";
            signature = _signature;
            lang = "vi";
        }
    }
}

