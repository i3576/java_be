package com.example.Java_BE.service;

import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface ProductService {
    Object getAllProduct();
    Object addProduct(Product product, String id);
    Object addSpecification(Specification specification, String id);
    Object getSpecificationForProductID( String id);
    Object getInfoByProductID(String id);
    Object getProductBySpecificationId(String id);
    Object getTop8Sell();
    Object getProductByParams(String category,Integer minPrice,Integer maxPrice,Float minRating,Float maxRating,Integer minBattery,Integer maxBattery);
    Object addNewProduct(Product product);
    Object updateExistProduct(Product product);
    Object deleteProduct(String id);
}
