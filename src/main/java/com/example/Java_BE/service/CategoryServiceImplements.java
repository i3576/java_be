package com.example.Java_BE.service;

import com.example.Java_BE.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Date;

@Repository
public class CategoryServiceImplements implements CategoryService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ProductService productService;

    @Override
    public Object addCategory(Category category) {
        Query q = new Query();
        q.addCriteria(Criteria.where("name").is(category.getName()));
        Category exist = mongoTemplate.findOne(q, Category.class);
        if (exist == null) {
            mongoTemplate.save(category);
            return Collections.singletonMap("res", "success");
        } else
            return Collections.singletonMap("res", "failure");
    }

    @Override
    public Object getExistCategories() {
        return mongoTemplate.findAll(Category.class);
    }
}
