package com.example.Java_BE.service;

import com.example.Java_BE.model.Category;
import com.example.Java_BE.model.Product;
import com.example.Java_BE.model.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class ProductServiceImplements implements ProductService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Object getAllProduct() {
        return mongoTemplate.findAll(Product.class);
    }

    @Override
    public Object addProduct(Product product, String id) {
        mongoTemplate.save(product);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object addSpecification(Specification specification, String id) {
        specification.setCreateAt(new Date());
        specification.setUpdateAt(new Date());
        mongoTemplate.save(specification);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object getSpecificationForProductID(String id) {
        Query q = new Query();
        q.addCriteria(Criteria.where("productId").is(id));
        return mongoTemplate.find(q,Specification.class);
    }

    @Override
    public Object getInfoByProductID(String id) {
        return mongoTemplate.findById(id,Product.class);
    }

    @Override
    public Object getProductBySpecificationId(String id){
        return mongoTemplate.findById(id,Specification.class);
    }

    @Override
    public Object getTop8Sell() {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "sold"));
        query.limit(8);
        return mongoTemplate.find(query,Product.class);
    }

    @Override
    public Object getProductByParams(String category,Integer minPrice,Integer maxPrice,Float minRating,Float maxRating,Integer minBattery,Integer maxBattery) {
        Criteria criteria = new Criteria();

        if (category != null) {
            criteria = criteria.and("category").is(category);
        }

        if (minPrice != null) {
            criteria = criteria.and("price").gte(minPrice.intValue());
        }
        if (maxPrice != null) {
            criteria = criteria.and("price").lte(maxPrice.intValue());
        }

        if (minRating != null) {
            criteria = criteria.and("rating").gte(minRating.floatValue());
        }
        if (maxRating != null) {
            criteria = criteria.and("rating").lte(maxRating.floatValue());
        }

        if (minBattery != null) {
            criteria = criteria.and("battery").gte(minBattery.intValue());
        }
        if (maxBattery != null) {
            criteria = criteria.and("battery").lte(maxBattery.intValue());
        }

        Query q = new Query(criteria);
        return mongoTemplate.find(q, Product.class);
    }

    @Override
    public Object addNewProduct(Product product) {
        mongoTemplate.save(product);
        return Collections.singletonMap("statusCode", 201);
    }

    @Override
    public Object updateExistProduct(Product product) {
        Product temp = mongoTemplate.findById(product.get_id(),Product.class);
        if(temp!=null){
            product.setCategoryId(temp.getCategoryId());
            mongoTemplate.save(product);
            return Collections.singletonMap("statusCode", 201);
        }
        else{
            return Collections.singletonMap("statusCode", 400);
        }
    }

    @Override
    public Object deleteProduct(String id) {
        Product product = mongoTemplate.findById(id,Product.class);

        List<Specification> speList = mongoTemplate.findAll(Specification.class);

        for(Specification item:speList){
            if(item.getProductId().equals(id))
            {
                mongoTemplate.remove(item);
            }
        }

        mongoTemplate.remove(product);
        return Collections.singletonMap("statusCode", 201);
    }
}
